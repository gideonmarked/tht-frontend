(function() {
  document.getElementById("form").addEventListener("submit", onFormSubmit);

  //handle form submissions
  async function onFormSubmit(event) {
    event.preventDefault();

    //test if the fields are valid
    if( !validateName( document.querySelector("[name=full_name]").value ) || !validateEmail( document.querySelector("[name=email]").value ) )
    {
      alert('The fields should be in correct format and should not be empty');
    } else {

      //set the field values to variable
      const data = {
        full_name: document.querySelector("[name=full_name]").value,
        email: document.querySelector("[name=email]").value
      };

      //fetch request to the backend api for creating users
      const response = await fetch(document.getElementById("form").getAttribute("action"), {
        method: 'POST',
        cache: 'no-cache',
        mode: 'no-cors',
        credentials: 'same-origin',
        headers: {
          'Content-Type': 'application/json'
        },
        redirect: 'follow',
        referrerPolicy: 'no-referrer',
        body: new URLSearchParams(data)
      });

      //hide the form and show thank you message
      document.querySelector("#message").style.display = "block";
      document.getElementById("form").style.display = "none";

      //add click event to message button to dismiss it
      document.querySelector("#message button").addEventListener('click',onMessageDismiss);

      //clear field values
      document.querySelector("[name=full_name]").value = "";
      document.querySelector("[name=email]").value = "";
    }
  }

  //clear field values
  function onMessageDismiss(event) {
    //remove click event to message button to dismiss it
    document.querySelector("#message button").removeEventListener('click',onMessageDismiss);

    //show the form and hide thank you message
    document.querySelector("#message").style.display = "none";
    document.getElementById("form").style.display = "block";
  }

  //method for testing the values of full_name field
  function validateName(full_name) 
  {
    return (full_name !== null && full_name !== "");
  }

  //method for testing the values of email field
  function validateEmail(mail) 
  {
    return (/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/.test(mail));
  }
})();